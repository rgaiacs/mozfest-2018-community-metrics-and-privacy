# Proposal

## Facilitator information

### Your information

- First name

  Raniere

- Surname

  Silva

- Email address

  raniere@rgaiacs.com

- Organisation or affiliation

  Software Sustainability Institute

- What country do you currently reside in?

  United Kingdom

- Twitter handle

  @rgaiacs

- GitHub handle

  @rgaiacs

### Additional facilitator's information

- First name

Georg   

- Surname

Link

- Email address

glink@unomaha.edu

- Organisation or affiliation

University of Nebraska at Omaha

- What country do you currently reside in?

U.S.A.

- Twitter handle

@georglink

- GitHub handle

@georglink

### Additional facilitator's information

- First name

Jona

- Surname

Azizaj

- Email address

jona@azizaj.com

- Organisation or affiliation

Fedora Project

- What country do you currently reside in?

Albania

- Twitter handle

@jonatoni

- GitHub handle

@jonatoni


## Festival spaces

- What space do feel your session will best contribute to?

  Openness

- Is there an alternate space your session could contribute to?

  Privacy and Security

## Language of your session

- Do you wish to propose or deliver your session in another language?

  No

## Describe your session

- What is the name of your session?

  Open Community Metrics and Privacy

- What will happen in your session?

  We will discuss what data should **and must not** be collected, aggregated and made public
  when doing community metrics analysis.
  Participants will engage in small group and whole group discussions.

- What is the goal or outcome of your session?

  The goal of this session is data gathering and discussion of ideals and values. 
  As outcome we will write a blog post after MozFest
  based on the notes of the session.

- After the festival, how will you and your participants take the learning and activities forward?

  Outcomes of this session will be shared with the [CHAOSS](https://chaoss.community/) (Community Health Analytics OSS) Project.
  The community 

- How will you deal with varying numbers of participants in your session? What if 30 participants attend? What if there are 3?

  As a data gathering session,
  we plan to split up into small groups 
  that will compare notes at the end of the session.
  This way we can work with any number of participants.

## Session formats

- Choose from the formats below that most suits your session delivery

  Learning Forum

- How much time you will need?

  90 mins

## Additional support

- If your session requires additional materials or electronic equipment, please outline your needs.

  A moderator kit with notecards and pens for small group discussions would be helpful.

- Do you qualify yourself as such?

  No.

  
